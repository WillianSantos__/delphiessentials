unit uExample_If;

interface

uses
   FMX.Controls,
   FMX.Controls.Presentation,
   FMX.Dialogs,
   FMX.Forms,
   FMX.Graphics,
   FMX.Memo,
   FMX.ScrollBox,
   FMX.StdCtrls,
   FMX.Types,
   System.Classes,
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Variants,
   FMX.Edit;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      pnlMain: TPanel;
      edtMain: TEdit;
      procedure btnMainClick(Sender: TObject);
      procedure FormCreate(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.FormCreate(Sender: TObject);
begin
   // For�a somente n�meros
   edtMain.FilterChar := '0123456789';

   // Limita quantidade de n�meros
   edtMain.MaxLength := 8;
end;

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   numeroInteiro: Integer;

const
   numeroComparacao = 16;

begin
   // Se for vazio
   if (edtMain.Text = EmptyStr) then
      Exit;

   // Se n�o for vazio
   if not(edtMain.Text = EmptyStr) then
      numeroInteiro := edtMain.Text.ToInteger; // New

   // if not(edtMain.Text = '') then  // Old
   // numeroInteiro := StrToInt(edtMain.Text);  // Old

   mmoMain.Lines.Clear;

   {$REGION 'If'}
   { todo: Maior }
   if (numeroInteiro > numeroComparacao) then
   begin
      mmoMain.Lines.Add('[1.1] ' + numeroInteiro.ToString + ' � maior do que ' + numeroComparacao.ToString); // New
      mmoMain.Lines.Add('[1.2] ' + IntToStr(numeroInteiro) + ' � maior do que ' + IntToStr(numeroComparacao)); // Old
   end;

   { todo: Menor }
   if not(numeroInteiro < numeroComparacao) then
      mmoMain.Lines.Add('[2] ' + numeroInteiro.ToString + ' n�o � menos que ' + numeroComparacao.ToString);

   { todo: Maior ou Igual }
   if (numeroInteiro >= numeroComparacao) then
      mmoMain.Lines.Add('[3] ' + numeroInteiro.ToString + ' � maior ou igual a ' + numeroComparacao.ToString);

   { todo: Menor ou Igual }
   if (numeroInteiro <= numeroComparacao) then
      mmoMain.Lines.Add('[4] ' + numeroInteiro.ToString + ' � menor ou igual a ' + numeroComparacao.ToString);

   { todo: Diferente }
   if (numeroInteiro <> numeroComparacao) then
      mmoMain.Lines.Add('[5] ' + numeroInteiro.ToString + ' � diferente de ' + numeroComparacao.ToString);

   { todo: Igual }
   if (numeroInteiro = numeroComparacao) then
      mmoMain.Lines.Add('[6] ' + numeroInteiro.ToString + ' � igual a ' + numeroComparacao.ToString);

   {$ENDREGION}
end;

end.
