object IWFrmIntraWeb: TIWFrmIntraWeb
  Left = 0
  Top = 0
  Width = 555
  Height = 400
  RenderInvisibleControls = False
  AllowPageAccess = True
  ConnectionMode = cmAny
  OnCreate = IWAppFormCreate
  Background.Fixed = False
  LayoutMgr = tpHtmlMain
  HandleTabs = False
  LeftToRight = True
  LockUntilLoaded = True
  LockOnSubmit = True
  StyleSheet.Filename = 
    'C:\TREINAMENTO\DelphiEssentials\APP\Example_03\template\style.cs' +
    's'
  ShowHint = True
  XPTheme = True
  DesignLeft = 8
  DesignTop = 8
  object edtFirst: TIWEdit
    AlignWithMargins = True
    Left = 0
    Top = 16
    Width = 555
    Height = 21
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    ZIndex = 1
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    NonEditableAsLabel = True
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    FriendlyName = 'edtFirst'
    SubmitOnAsyncEvent = True
    TabOrder = 0
    Text = '4'
    ExplicitLeft = 10
    ExplicitTop = 33
    ExplicitWidth = 521
  end
  object lblSecound: TIWLabel
    AlignWithMargins = True
    Left = 0
    Top = 37
    Width = 555
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    ZIndex = 2
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    HasTabOrder = True
    FriendlyName = 'IWLabel1'
    Caption = 'Secound'
    RawText = True
    ExplicitLeft = 40
    ExplicitTop = 80
    ExplicitWidth = 53
  end
  object edtSecound: TIWEdit
    AlignWithMargins = True
    Left = 0
    Top = 53
    Width = 555
    Height = 21
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    ZIndex = 3
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    NonEditableAsLabel = True
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    FriendlyName = 'edtSecound'
    SubmitOnAsyncEvent = True
    TabOrder = 1
    Text = '8'
    ExplicitLeft = 8
    ExplicitTop = 81
    ExplicitWidth = 537
  end
  object btnSum: TIWButton
    AlignWithMargins = True
    Left = 0
    Top = 74
    Width = 555
    Height = 25
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'btn btn-primary'
    ZIndex = 4
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    Caption = 'Sum'
    Color = clBtnFace
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    FriendlyName = 'btnSum'
    TabOrder = 2
    OnClick = btnSumClick
    ExplicitLeft = 8
    ExplicitTop = 115
    ExplicitWidth = 537
  end
  object lblResult: TIWLabel
    AlignWithMargins = True
    Left = 0
    Top = 99
    Width = 555
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    ZIndex = 5
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    HasTabOrder = True
    FriendlyName = 'lblResult'
    Caption = 'Result'
    RawText = True
    ExplicitLeft = 32
    ExplicitTop = 184
    ExplicitWidth = 38
  end
  object edtResult: TIWEdit
    AlignWithMargins = True
    Left = 0
    Top = 115
    Width = 555
    Height = 21
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    ZIndex = 6
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    Editable = False
    NonEditableAsLabel = True
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    FriendlyName = 'edtResult'
    SubmitOnAsyncEvent = True
    TabOrder = 3
    Text = '0'
    ExplicitLeft = 32
    ExplicitTop = 206
    ExplicitWidth = 121
  end
  object lblFirst: TIWLabel
    AlignWithMargins = True
    Left = 0
    Top = 0
    Width = 555
    Height = 16
    Margins.Left = 0
    Margins.Top = 0
    Margins.Right = 0
    Margins.Bottom = 0
    Align = alTop
    Css = 'form-control'
    RenderSize = False
    StyleRenderOptions.RenderSize = False
    StyleRenderOptions.RenderPosition = False
    StyleRenderOptions.RenderFont = False
    StyleRenderOptions.RenderZIndex = False
    StyleRenderOptions.RenderVisibility = False
    StyleRenderOptions.RenderStatus = False
    StyleRenderOptions.RenderAbsolute = False
    StyleRenderOptions.RenderPadding = False
    StyleRenderOptions.RenderBorder = False
    Font.Color = clNone
    Font.Size = 10
    Font.Style = []
    HasTabOrder = True
    FriendlyName = 'lblFirst'
    Caption = 'First'
    RawText = True
    ExplicitLeft = 10
    ExplicitTop = 8
    ExplicitWidth = 521
  end
  object tpHtmlMain: TIWTemplateProcessorHTML
    TagType = ttIntraWeb
    Templates.Default = 'template.html'
    Left = 248
    Top = 176
  end
  object ppMain: TPageProducer
    HTMLDoc.Strings = (
      '<!DOCTYPE html>'
      '<html lang="en">'
      '  <head>'
      '    <meta charset="utf-8">'
      '    <meta http-equiv="X-UA-Compatible" content="IE=edge">'
      
        '    <meta name="viewport" content="width=device-width, initial-s' +
        'cale=1, shrink-to-fit=no">'
      '    <meta name="description" content="Example IntraWeb">'
      '    <meta name="author" content="Willian Santos">'
      ''
      '    <!-- Bootstrap CSS -->'
      
        '    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.' +
        'com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-Wsk' +
        'haSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" c' +
        'rossorigin="anonymous">'
      ''
      '    <title>Example IntraWeb with Bootstrap</title>'#9
      '</head>'
      '<body>    '
      
        '    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg' +
        '-dark">'
      
        '      <a class="navbar-brand" href="#">Example IntraWeb with Boo' +
        'tstrap</a>  '
      #9'</nav>'
      #9'<br>'
      #9'<br>'#9
      #9'<br>'
      
        #9'<div class="alert alert-warning alert-dismissible fade show" ro' +
        'le="alert">'
      #9#9'<strong>'
      #9#9#9'IntraWeb with Bootstrap and Delphi.'
      #9#9'</strong>'
      #9#9'<br>'
      #9#9'Click on button "Sum" for execute the method!'
      
        #9#9'<button type="button" class="close" data-dismiss="alert" aria-' +
        'label="Close">'
      #9#9#9'<span aria-hidden="true">&times;</span>'
      #9#9'</button>'
      #9'</div>'#9
      #9
      #9'<div class="container">'
      #9'<form>'#9
      '        <div class="form-group row">'
      
        '            <label class="col-sm-12 col-md-12 col-lg-12 col-form' +
        '-label">First</label>'
      '            <div class="col-sm-12 col-md-12 col-lg-12">'
      '                {%edtFirst%}'
      '            </div>'
      '        </div>'
      ''
      '        <div class="form-group row">'
      
        '            <label class="col-sm-12 col-md-12 col-lg-12 col-form' +
        '-label">Secound</label>'
      '            <div class="col-sm-12 col-md-12 col-lg-12">'
      '                {%edtSecound%}'
      '            </div>'
      '        </div>'
      ''
      '        <div class="form-group row">'
      '            <div class="col-sm-1 col-md-1 col-lg-1">'
      '                {%btnSum%}'
      '            </div>'
      '        </div>'
      ''
      '        <div class="form-group row">'
      
        '            <label class="col-sm-12 col-md-12 col-lg-12 col-form' +
        '-label">Result</label>'
      '            <div class="col-sm-12 col-md-12 col-lg-12">'
      '                {%edtResult%}'
      '            </div>'
      '        </div>'
      '    </form>'
      #9'</div>'
      '    <!-- Optional JavaScript -->'
      '    <!-- jQuery first, then Popper.js, then Bootstrap JS -->'
      
        '    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.j' +
        's" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp' +
        '4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>'
      
        '    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.j' +
        's/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J' +
        '3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="ano' +
        'nymous"></script>'
      
        '    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.' +
        '1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk' +
        '/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonym' +
        'ous"></script>'
      '  </body>'
      '</html>')
    Left = 248
    Top = 224
  end
end
