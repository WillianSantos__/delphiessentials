unit uExample_IntraWeb;

interface

uses
   Classes,
   IWAppForm,
   IWApplication,
   IWBaseContainerLayout,
   IWBaseControl,
   IWBaseHTMLControl,
   IWBaseLayoutComponent,
   IWColor,
   IWCompButton,
   IWCompEdit,
   IWCompLabel,
   IWCompMemo,
   IWContainer,
   IWContainerLayout,
   IWControl,
   IWHTML40Container,
   IWHTMLContainer,
   IWRegion,
   IWTemplateProcessorHTML,
   IWTypes,
   IWVCLBaseContainer,
   IWVCLBaseControl,
   IWVCLComponent,
   IWHTMLTag,
   ServerController,
   SysUtils,
   Vcl.Controls,
   Vcl.Forms,
   Web.HTTPApp,
   Web.HTTPProd;

type
   TIWFrmIntraWeb = class(TIWAppForm)
      tpHtmlMain: TIWTemplateProcessorHTML;
      edtFirst: TIWEdit;
      lblSecound: TIWLabel;
      edtSecound: TIWEdit;
      btnSum: TIWButton;
      lblResult: TIWLabel;
      edtResult: TIWEdit;
      lblFirst: TIWLabel;
      ppMain: TPageProducer;
      procedure btnSumClick(Sender: TObject);
      procedure IWAppFormCreate(Sender: TObject);
   public
   end;

implementation

{$R *.dfm}

procedure TIWFrmIntraWeb.btnSumClick(Sender: TObject);
var
   vFirst: Double;
   vSecound: Double;
   vResult: Double;
begin
   vFirst := StrToFloat(StringReplace(edtFirst.Text, ',', '.', [rfReplaceAll]));
   vSecound := StrToFloat(StringReplace(edtSecound.Text, ',', '.', [rfReplaceAll]));
   vResult := vFirst + vSecound;
   edtResult.Text := FloatToStr(vResult);
end;

procedure TIWFrmIntraWeb.IWAppFormCreate(Sender: TObject);
var
   s: string;
   sl: TStringList;
begin
   FormatSettings.DecimalSeparator := '.';

   s := ExtractFilePath(ParamStr(0)) + 'Templates\template.html';

   if not FileExists(s) then
   begin
      sl := TStringList.Create;
      try
         sl.Text := ppMain.HTMLDoc.Text;
         sl.SaveToFile(s);
      finally
         FreeAndNil(sl);
      end;
   end;
end;

initialization

TIWFrmIntraWeb.SetAsMainForm;

end.
