unit ServerController;

interface

uses
   SysUtils,
   Classes,
   IWServerControllerBase,
   IWBaseForm,
   HTTPApp,
   // For OnNewSession Event
   UserSessionUnit,
   IWApplication,
   IWAppForm;

type
   TIWServerController = class(TIWServerControllerBase)
      procedure IWServerControllerBaseNewSession(ASession: TIWApplication);
      procedure IWServerControllerBaseConfig(Sender: TObject);

   private

   public
   end;

function UserSession: TIWUserSession;
function IWServerController: TIWServerController;

implementation

{$R *.dfm}

uses
   IWInit,
   IWGlobal;

function IWServerController: TIWServerController;
begin
   Result := TIWServerController(GServerController);
end;

function UserSession: TIWUserSession;
begin
   Result := TIWUserSession(WebApplication.Data);
end;

procedure TIWServerController.IWServerControllerBaseConfig(Sender: TObject);
begin
   Self.CharSet := 'utf-8';
   Self.LicenseTracking := False;
   Self.HistoryEnabled := False;
   Self.BackButtonOptions.Mode := TIWBackButtonMode.bmEnable;

   Self.LogCommandEnabled := False;
   Self.LogSessionEvents := False;
   Self.ExceptionLogger.Enabled := False;

   Self.Compression.AllowDeflate := True;
   Self.Compression.AllowGZip := True;
   Self.Compression.Level := 6;
   Self.Compression.MinSize := 512;
   Self.Compression.PreCompress := True;
   Self.Compression.Enabled := True;

   Self.CookieOptions.CookieNameSuffix := EmptyStr;
   Self.CookieOptions.HttpOnly := False;
   Self.CookieOptions.Secure := False;
   Self.CookieOptions.SessionCookies := False;
   Self.CookieOptions.UseCookies := True;

   Self.AllowMultipleSessionsPerUser := False;
   Self.AuthBeforeNewSession := False;
   Self.RestartExpiredSession := True;
   Self.SecurityOptions.CheckSameIP := False;
   Self.SecurityOptions.CheckSameUA := False;
   Self.SecurityOptions.ShowSecurityErrorDetails := False;
   Self.SessionTimeout := 7200;
   Self.ShowStartParams := False;
   Self.IECompatibilityMode := 'IE=edge';
   Self.HttpKeepAlive := True;
   Self.PageTransitions := True;
   Self.JavaScriptOptions.RenderjQuery := False;

   Self.SearchEngineOptions.RedirectToContentHandler := True;
   Self.SearchEngineOptions.ContentHandlerPath := 'SearchEngineRequest';
end;

procedure TIWServerController.IWServerControllerBaseNewSession(ASession: TIWApplication);
begin
   ASession.Data := TIWUserSession.Create(nil);
end;

initialization

TIWServerController.SetServerControllerClass;

end.
