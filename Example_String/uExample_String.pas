﻿unit uExample_String;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.Controls.Presentation,
   FMX.StdCtrls;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   s: string;
   ch1, ch2: Char;
begin
   // s := 'Hello';
   s := '您好';

   ch1 := s[1];
   ch2 := s[2];

   mmoMain.Lines.Clear;
   mmoMain.Lines.Add('My String: ' + s);
   mmoMain.Lines.Add('Char 1: ' + ch1);
   mmoMain.Lines.Add('Char 2: ' + ch2);

end;

end.
