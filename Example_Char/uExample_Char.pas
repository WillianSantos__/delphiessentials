unit uExample_Char;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.Controls.Presentation,
   FMX.StdCtrls;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: integer;
   s: string;
   f: string;
begin
   mmoMain.Lines.Clear;

   for i := 0 to 999999 do
   begin
      s := Chr(i);
      f := FormatFloat('000000', i);

      if not(s = EmptyStr) then
         mmoMain.Lines.Add('#' + f + ': [ ' + s + ' ]');
   end;
end;

end.
