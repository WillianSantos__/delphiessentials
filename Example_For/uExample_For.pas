unit uExample_For;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.Controls.Presentation,
   FMX.StdCtrls;

type
   TFrmMain = class(TForm)
    btnMain: TButton;
    mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: integer;
begin
   mmoMain.Lines.Clear;

   { todo: Count 4 }
   mmoMain.Lines.Add('Count 4');

   for i := 0 to 3 do
      mmoMain.Lines.Add(i.ToString);

   { todo: Count 3 }
   mmoMain.Lines.Add('Count 3');
   for i := 0 to Pred(3) do
      mmoMain.Lines.Add(i.ToString);

   { todo: Begin with 1 }
   mmoMain.Lines.Add('i := 1');
   for i := 1 to 3 do
      mmoMain.Lines.Add(i.ToString);

   { todo: DownTo with 3 }
   mmoMain.Lines.Add('i := 3 - DownTo');
   for i := 3 downto 1 do
      mmoMain.Lines.Add(i.ToString);
end;

end.
