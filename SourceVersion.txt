﻿-- Usando Github ou https://bitbucket.org/

-- Configurando Dados do Github
git config --global user.name "Willian Santos"
git config --global user.email "williansantos__@live.com"

-- Iniciando um Projeto para o Github, dentro da pasta do projeto execute o comando
git init
git config core.autocrlf
git add .
git commit -m "Star Project (Commit)"

-- Para subir um projeto no Github adicione no repositório no site, depois execute o comando para subir no repositório
git remote add origin https://WillianSantos__@bitbucket.org/WillianSantos__/delphiessentials.git
git push -u -f origin master

— Para limpar 
git remote rm origin
