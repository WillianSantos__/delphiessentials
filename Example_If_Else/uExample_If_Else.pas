unit uExample_If_Else;

interface

uses
   FMX.Controls,
   FMX.Controls.Presentation,
   FMX.Dialogs,
   FMX.Forms,
   FMX.Graphics,
   FMX.Memo,
   FMX.ScrollBox,
   FMX.StdCtrls,
   FMX.Types,
   System.Classes,
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Variants;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: Double;
   j: Double;
   k: Double;
begin
   k := Random(9);
   i := Random(99);
   j := Random(999);

   if (i > j) then
      k := i / j
   else if (i < j) then
      k := i + j
   else if (i = j) then
      k := 0
   else
   begin
      i := 36;
      j := 8;
      k := (i + j);
   end;

   mmoMain.Lines.Clear;
   mmoMain.Lines.Add('[k]: ' + k.ToString);
   mmoMain.Lines.Add('[i]: ' + i.ToString);
   mmoMain.Lines.Add('[j]: ' + j.ToString);
end;

end.
