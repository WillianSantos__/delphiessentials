unit uExample_While;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.Controls.Presentation,
   FMX.StdCtrls;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: Integer;
begin
   i := 0;

   mmoMain.Lines.Clear;

   { todo: While Do }
   while not(i > 10) do
   begin
      mmoMain.Lines.Add(i.ToString);
      Inc(i); // New
      // i := i + 1; // Old
   end;
end;

end.
