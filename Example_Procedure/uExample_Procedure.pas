unit uExample_Procedure;

interface

uses
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Classes,
   System.Variants,
   FMX.Types,
   FMX.Controls,
   FMX.Forms,
   FMX.Graphics,
   FMX.Dialogs,
   FMX.ScrollBox,
   FMX.Memo,
   FMX.Controls.Presentation,
   FMX.StdCtrls;

type
   TFrmMain = class(TForm)
      btnProcedure: TButton;
      mmProcedure: TMemo;
      procedure btnProcedureClick(Sender: TObject);
   private
      procedure NewProc(I: Integer; var B: Boolean);
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.NewProc(I: Integer; var B: Boolean);
begin
   if (I mod 2 = 0) then
      B := True
   else
      B := False;
end;

procedure TFrmMain.btnProcedureClick(Sender: TObject);
var
   myInt: Integer;
   myBool: Boolean;
begin
   myInt := 3;

   NewProc(myInt, myBool);

   if myBool then
      mmProcedure.Lines.Add('N�mero Par')
   else
      mmProcedure.Lines.Add('N�mero Impar');
end;

end.
