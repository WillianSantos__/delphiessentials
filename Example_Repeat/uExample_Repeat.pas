unit uExample_Repeat;

interface

uses
   FMX.Controls,
   FMX.Controls.Presentation,
   FMX.Dialogs,
   FMX.Forms,
   FMX.Graphics,
   FMX.Memo,
   FMX.ScrollBox,
   FMX.StdCtrls,
   FMX.Types,
   System.Classes,
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Variants;

type
   TFrmMain = class(TForm)
    btnMain: TButton;
    mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: integer;
begin
   i := 9;

   repeat
      mmoMain.Lines.Add(i.ToString);
      Dec(i);
   until (i = 0);

   mmoMain.Lines.Add(EmptyStr);

   i := 1;

   repeat
      mmoMain.Lines.Add(i.ToString);
      Inc(i);
   until (i = 10);
end;

end.
