unit uExample_Case;

interface

uses
   FMX.Controls,
   FMX.Controls.Presentation,
   FMX.Dialogs,
   FMX.Forms,
   FMX.Graphics,
   FMX.Memo,
   FMX.ScrollBox,
   FMX.StdCtrls,
   FMX.Types,
   System.Classes,
   System.SysUtils,
   System.Types,
   System.UITypes,
   System.Variants;

type
   TFrmMain = class(TForm)
      btnMain: TButton;
      mmoMain: TMemo;
      procedure btnMainClick(Sender: TObject);
   private
      { Private declarations }
   public
      { Public declarations }
   end;

var
   FrmMain: TFrmMain;

implementation

{$R *.fmx}

procedure TFrmMain.btnMainClick(Sender: TObject);
var
   i: integer;
begin
   i := Random(101);

   case i of
      1 .. 50:
         begin
            mmoMain.Lines.Add('Baixo 1');
            mmoMain.Lines.Add('Baixo 2');
         end;
      51 .. 90:
         mmoMain.Lines.Add('Cima');
      0, 91 .. 99:
         mmoMain.Lines.Add('Fora');
   else
      mmoMain.Lines.Add('Sem Limite');
   end;

   mmoMain.Lines.Add('N�mero aleat�ro: ' + i.ToString);
end;

end.
